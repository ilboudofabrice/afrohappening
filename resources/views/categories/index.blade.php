@extends('layouts.master')

@section('header')
    <h2>Catégories</h2>
   
    <div class="container">
        <a href="{{ url('categories/create') }}" class="btn btn-outline-primary">
            Nouvelle catégorie
        </a>
    </div>
@stop

@section('content')
    <div class="container" style="margin-top: 20px;">
        <ul class="list-group">
            @foreach ($categories as $category)
                <li class="list-group-item list-group-item-action category">
                    <a href="{{ url('categories/'.$category->id) }}">
                        <strong>{{ $category->name }}</strong>
                    </a>
                    <div class="btn-group mr-2 float-right" role="group" aria-label="First group">
                        <a href="{{ url('categories/'.$category->id.'/edit') }}" class="btn btn-primary btn-sm" role="button" aria-disabled="true">Modifier</a>
                        <a href="{{ url('categories/'.$category->id.'/delete') }}" class="btn btn-danger btn-sm" role="button" aria-disabled="true">Supprimer</a>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@stop