<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Afro Happening</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    </head> 
    <body style="margin-bottom: 100px;">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Afro Happening</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                        <li class="nav-item nav-link active {{ (Request::is('/') ? 'active' : '') }}">
                            <a href="{{ url('') }}"><i class="fa fa-home"></i>Acceuil</a>
                        </li>
                        <li class="nav-item nav-link active {{ (Request::is('events') ? 'active' : '') }}">
                            <a href="{{ url('events') }}">Evènements</a>
                        </li>
                        <li class="nav-item nav-link active {{ (Request::is('categories') ? 'active' : '') }}">
                            <a href="{{ url('categories') }}">Catégories</a>
                        </li>
                        <li class="nav-item nav-link active {{ (Request::is('organizers') ? 'active' : '') }}">
                            <a href="{{ url('organizers') }}">Organisateurs</a>
                        </li>
                </ul>
            </div>
        </nav>
        
        <div class="container">
            <div class="page-header">
                @yield('header')
            </div>
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @yield('content')
        </div>
    </body>
</html>