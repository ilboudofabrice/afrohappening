<div class="form-group">
    {!! Form::label('title', 'Titre') !!}
    <div class="form-controls">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('description', 'Description') !!}
    <div class="form-controls">
        {!! Form::text('description', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('startDate', 'Date et heure de début') !!}
    <div class="form-controls">
        {!! Form::date('startDate', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('endDate', 'Date et heure de fin') !!}
    <div class="form-controls">
        {!! Form::date('endDate', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('category_id', 'Catégorie') !!}
    <div class="form-controls">
    {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('country_name', 'Pays') !!}
    <div class="form-controls">
        {!! Form::select('country_name', $countries, null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('eventPicture', 'Image') !!}
    <div class="form-controls">
        <input type="file" name="eventPicture" id=""/>
        {{--  {!! Form::file('eventPicture', array('class' => 'form-control', 'enctype' => 'multipart/form-data')) !!}  --}}
    </div>
</div>

<div class="form-group pull-left"></div>
    <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
</div>