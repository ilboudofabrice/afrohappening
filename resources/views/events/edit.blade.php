@extends('layouts.master')

@section('header')
    
    <h2>Modifier un évènement</h2>

@stop

@section('content')
    
    {!! Form::model($event, array('url' => '/events/'.$event->id, 'method' => 'put', 'enctype' => 'multipart/form-data')) !!}
    
    @include('partials.forms.event')
    
    {!! Form::close() !!}
@stop