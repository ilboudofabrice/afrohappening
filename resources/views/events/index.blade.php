@extends('layouts.master')

@section('header')
    <h2>Evènements</h2>
   
    <div class="container">
        <a href="{{ url('events/create') }}" class="btn btn-outline-primary">
            Nouveau évènement
        </a>
    </div>
@stop

@section('content')
    <div class="events-list card-deck">
        @foreach ($events as $event)
            <div class="card event-card" style="width: 18rem; margin-top: 40px;">
                <div class="card-img-top event-card-img">
                    <img class="card-img-top" src="{{ URL::asset('images/').'/'.$event->pictureName }}" alt="Card image cap">
                    <span class="poster-card-event-price">FREE</span>
                </div>
                <div class="card-body">
                    <time class="poster-card-event-date"> {{ $event->startDate }}</time>
                    <h5 class="card-title poster-card-event-title">{{ $event->title }}</h5>
                    {{--  <p class="card-text">{{ $event->description }}</p>  --}}
                    <a href="{{ url('events/'.$event->id) }}" class="btn btn-primary btn-sm">Détails</a>
                </div>
            </div>
            {{--  <div class="event">
                <a href="{{ url('events/'.$event->id) }}">
                    <strong>{{ $event->title }}</strong>
                </a>
            </div>  --}}
        @endforeach
    </div>
@stop