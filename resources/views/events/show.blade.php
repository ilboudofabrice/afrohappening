@extends('layouts.master')

@section('header')
    <a href="{{ url('/events') }}">Back to overview</a>
    <h2>
        {{ $event->title }} 
    </h2>
    <h4></h4>
        {{ $event->description }} 
    </h4>
    <br><br>
    <a href="{{ url('events/'.$event->id.'/edit') }}">
        <span class="glyphicon glyphicon-edit"></span> 
        Edit
    </a>
    <a href="{{ url('events/'.$event->id.'/delete') }}">
        <span class="glyphicon glyphicon-trash"></span>
        Delete
    </a>
    {{--  <p>Last edited: {{ $category->updated_at->diffForHumans() }}</p>  --}}
@stop