@extends('layouts.master')

@section('header')
    
    <h2>Nouveau évènement</h2>

@stop

@section('content')
    
    {!! Form::open(['url' => '/events','enctype' => 'multipart/form-data']) !!}
    
        @include('partials.forms.event')
    
    {!! Form::close() !!}

@stop