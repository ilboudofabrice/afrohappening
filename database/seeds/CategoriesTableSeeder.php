<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(['name' => 'Conférence', 'description' => '']);
        DB::table('categories')->insert(['name' => 'Dinner ou Gala', 'description' => '']);
        DB::table('categories')->insert(['name' => 'Festival', 'description' => '']);
        DB::table('categories')->insert(['name' => 'Evènement de réseautage', 'description' => '']);
        DB::table('categories')->insert(['name' => 'Séminaire', 'description' => '']);
        DB::table('categories')->insert(['name' => 'Autre', 'description' => '']);
    }
}
