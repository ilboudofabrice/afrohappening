<?php

namespace AfroHappening;

use Illuminate\Database\Eloquent\Model;

use AfroHappening\Event;

class Category extends Model
{
    protected $fillable = ['name', 'description'];

    public function events() {
        return $this->hasMany('Event');
    }
}
