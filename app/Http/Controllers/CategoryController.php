<?php

namespace AfroHappening\Http\Controllers;

use Illuminate\Http\Request;

use AfroHappening\Category;

use Okipa\LaravelBootstrapTableList\TableList;

class CategoryController extends Controller
{
    public function index()
    {
        // we instantiate a table list in the news controller
        // $table = app(TableList::class)
        //         ->setModel(Category::class)
        //         ->setRoutes([
        //             'index' => ['alias' => 'categories.index', 'parameters' => []],
        //     ]);
        // // we add some columns to the table list
        // $table->addColumn('name')
        //       ->setTitle('Titre')
        //       ->sortByDefault()
        //       ->isSortable()
        //       ->isSearchable()
        //       ->useForDestroyConfirmation();
        
        // $table->addColumn('description')
        //       ->setTitle('Content')
        //       ->setStringLimit(30);
        
        // $table->addColumn('created_at')
        //       ->setTitle('Creation date')
        //       ->isSortable()
        //       ->setColumnDateFormat('d/m/Y H:i:s');

        $categories = Category::all();
        return view('categories.index')->with('categories', $categories);
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $category = Category::create($request->all());
        
        return redirect('categories')->withSuccess('Category has been created.');
    }

    public function show($id)
    {
        $category = Category::find($id);
        
        return view('categories.show')->with('category', $category);
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('categories.edit')->with('category', $category);
    }

    public function update(Request $request)
    {
        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->description = $request->description;
        
        $category->save();

        return redirect('categories')->withSuccess('Category has been updated.');
    }

    public function destroy(Request $request)
    {
        $category = Category::find($request->id);
        $category->delete();
        
        return redirect('categories')->withSuccess('Category has been deleted.');
    }
}
