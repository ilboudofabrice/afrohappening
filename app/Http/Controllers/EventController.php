<?php

namespace AfroHappening\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;

use GuzzleHttp\Exception\GuzzleException;

use GuzzleHttp\Client;

use AfroHappening\Event;

use AfroHappening\Category;

class EventController extends Controller
{
    public function index()
    {
        $events = Event::all();

        return view('events.index')->with('events', $events);
    }

    public function create()
    {
        $categories = Category::all();
        $countries = self::getCountries();

        return view('events.create')->with('categories', $categories->pluck('name', 'id'))
                                    ->with('countries', $countries);
    }

    public function store(Request $request)
    {
        $eventRequest = $request->all();

        //if there are an attached picture we will get it and store it on server
        //we will create a custom picture name and add to the event
        $pictureName = self::uploadPicture($request);

        // create new event object
        $event = new Event([
            'title'         => $eventRequest['title'],
            'description'   => $eventRequest['description'],
            'startDate'     => $eventRequest['startDate'],
            'endDate'       => $eventRequest['endDate'],
            'status'        => 'DRAFT',
            'pictureName'   => $pictureName,
            'category_id'   => $eventRequest['category_id']
        ]);

        $event->save();
        
        return redirect('events')->withSuccess('Event has been created.');
    }

    public function show($id)
    {
        $event = Event::find($id);
        
        return view('events.show')->with('event', $event);
    }

    public function edit($id)
    {
        $event = Event::find($id);
        $categories = Category::all();

        return view('events.edit')->with('event', $event)
                                  ->with('categories', $categories->pluck('name', 'id'));
    }

    public function update(Request $request)
    {
        $event = Event::find($request->id);
        if ($event->title != $request->title)
        {
            $event->title = $request->title;
        }
        if ($event->description != $request->description)
        {
            $event->description = $request->description;
        }
        if ($event->startDate != $request->startDate)
        {
            $event->startDate = $request->startDate;
        }
        if ($event->endDate != $request->endDate)
        {
            $event->endDate = $request->endDate;
        }
        /*
        if ($event->status != $request->status)
        {
            $event->status = $request->status;
        }
        */
        $pictureName = self::uploadPicture($request);
        if ($pictureName != "" && $event->pictureName != $pictureName) 
        {
            $event->pictureName = $pictureName;
        }
        
        $event->save();

        return redirect('events')->withSuccess('Event has been updated.');
    }

    public function destroy(Request $request)
    {
        $event = Event::find($request->id);
        $event->delete();
        
        return redirect('events')->withSuccess('Event has been deleted.');
    }


    function uploadPicture(Request $request) 
    {
        $picture = $request->file('eventPicture');
        $pictureName = "";
        if ($picture != null)
        {
            /*
            //validate picture
            $request->validate([
                'image' => 'required|image|max:2000',
                'category_id' => 'required|exists:categories,id',
                'description' => 'nullable|string|max:255',
            ]);
            */
            $pictureName = time().'.'.$picture->getClientOriginalExtension();
            $picture->move(public_path('images'), $pictureName);
        }

        return $pictureName;
    }

    function getCountries() 
    {
        $client = new Client();
        $res = $client->get('https://restcountries.eu/rest/v2/all');
        $responseCode = $res->getStatusCode(); // 200
        if ($responseCode == 200)
        {
            $countriesInJson = json_decode($res->getBody());
            $countriesLength = count($countriesInJson);
            $countriesNames = array();
            foreach ($countriesInJson as $country)
            {
                array_push($countriesNames, $country->name);
            }

            return $countriesNames;
        }
    }
}
