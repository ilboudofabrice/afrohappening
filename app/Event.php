<?php

namespace AfroHappening;

use Illuminate\Database\Eloquent\Model;

use AfroHappening\Category;

class Event extends Model
{
    protected $fillable = array('title', 'description', 'startDate', 'endDate', 'status', 'pictureName', 'category_id');
    
    // public function organizer()
    // {
    //     return $this->hasOne('Organizer');
    // }

    public function category()
    {
        return $this->hasOne('Category');
    }
}
