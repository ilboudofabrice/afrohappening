<?php

namespace AfroHappening\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Contracts\View\Factory as ViewFactory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(ViewFactory $view)
    {
        $view->composer('partials.forms.category', 'AfroHappening\Http\Views\Composers\CommonFormComposer');
        
        $view->composer('partials.forms.organizer', 'AfroHappening\Http\Views\Composers\CommonFormComposer');

        $view->composer('partials.forms.event', 'AfroHappening\Http\Views\Composers\CommonFormComposer');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
